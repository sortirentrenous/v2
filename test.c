#include <unistd.h>

void	ft_putchar(char c) {
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0') {
		ft_putchar(str[i]);
		i++;
	}
}

int main(int argc, char *argv[]) {
	int i;

	ft_putstr("Voici les arguments passe au prog ");
	ft_putstr(argv[0]);
	ft_putchar('\n');
	for (i = 1 ; i < argc ; i++) {
		ft_putstr(argv[i]);
		ft_putchar('\n');
	}
	ft_putstr("Prog by Cbourree\n");
}