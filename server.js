//Super Framework NodeJS, qui gère le MVC
var express = require('express');
// Charge le middleware de sessions
var session = require('cookie-session');
// Charge le middleware de gestion des paramètres (Variables passé en POST et GET)
var bodyParser = require('body-parser');
// Charge le middleware execute une commande linux (Pas besoin de le NPM)
var cp = require('child_process'); 
//Middleware pour MariaDB
var ClientDB = require('mariasql');
// Un Parseur d'url
var urlencodedParser = bodyParser.urlencoded({ extended: false });
// Charge le middleware pour gérer les requête HTTP
var http = require('http');
// Charge le middleware MQTT for user MQTT borker
const mqtt = require('mqtt')  


// Création de l'app
var app = express();

//Dossier public accecible à tous (donc comme ressource)
app.use(express.static(__dirname + '/public'));
//On utilise les sessions
app.use(session({secret: 'todotopsecret'}))


/* Pour chaque page on verifie si les objets session et msg
existent en session */
.use(function(req, res, next) {
    if (typeof(req.session.user) == 'undefined') {
        req.session.user = {
          ID: 0,
          pseudo: '',
          type: 0
        }
    }
    if (typeof(req.session.msg) == 'undefined') {
      req.session.msg = '';
    }
    next();
})

/* On affiche la todolist et le formulaire */
.get('/index', function(req, res) {
  SQL('SELECT * FROM etat_balance WHERE 1', {}, function(rows0) {
    SQL('SELECT * FROM etat_melangeur WHERE 1', {}, function(rows1) {
      SQL('SELECT * FROM etat_powermeter WHERE 1', {}, function(rows2) {
        SQL('SELECT * FROM etat_ram WHERE 1', {}, function(rows3) {
          SQL('SELECT * FROM cmd_melangeur WHERE 1', {}, function(rows4) {
            SQL('SELECT * FROM cmd_ram WHERE 1', {}, function(rows5) {
              SQL('SELECT * FROM set_alarmes WHERE 1', {}, function(rows6) {
                SQL('SELECT * FROM alarmes WHERE 1', {}, function(rows7) {
                  SQL('SELECT * FROM journal WHERE 1', {}, function(rows8) {
                    res.render('index.ejs', {user: req.session.user, msg: req.session.msg, etat_balance: rows0,
                                            etat_melangeur: rows1, etat_powermeter: rows2, etat_ram:rows3,
                                            cmd_melangeur: rows4, cmd_ram: rows5, set_alarmes: rows6,
                                            alarmes: rows7, journal:rows8});//On affiche la vue
                    req.session.msg = '';
                  });
                });
              });
            });
          });
        });
      });
    });
  });
})

// Le client Web demande la page de connexion
.get('/connexion', function(req, res) {
    //On envoie la vue de connexion
    res.render('connexion.ejs', 
    {user: req.session.user, msg: req.session.msg});
})

/* Le client web vient de nous transmettre le fomulaire de connexion en POST*/
.post('/traitementConnexion', urlencodedParser, function(req, res) {
    //Si les champs pseudo et mdp ontété transmit par formulaire
    if (req.body.pseudo != '' && req.body.mdp != '') { 
      SQL('SELECT * FROM utilisateurs WHERE login = :login',
          {login: req.body.pseudo}, function(rows) {
        // Oui on m'a inposé des mdp stockés en claire    
        if(rows.length > 0 && req.body.pseudo == rows[0].login && req.body.mdp == rows[0].password) {
          req.session.msg = 'Bienvenue ' + req.body.pseudo;
          req.session.user.ID = rows[0].ID;
          req.session.user.login = rows[0].login;
          req.session.user.type = rows[0].type; // Permission de l'utilisateur
          res.redirect('/index'); 
        } else {
          req.session.msg = 'Pseudo ou MDP invalide';
          res.redirect('/index');
        }
      });
    } else {
      req.session.msg = 'Pseudo ou MDP vide';
      res.redirect('/index');
    }
})

.get('/traitementDeconnexion', function(req, res) {
  req.session.msg = 'Merci d\'être passé';
  req.session.user.ID = 0;
  req.session.user.login = '';
  req.session.user.type = 0;
  res.redirect('/index');
})

/*

// On ajoute un élément à la liste de commande executées
.post('/cmd/ajouter/', urlencodedParser, function(req, res) {
    if (req.body.newcmd != '') { //Si un champ newcmd à été transmit par formulaire
        req.session.cmds.push(req.body.newcmd);
        cp.exec(req.body.newcmd, function(err, stdout, stderr) {  // par exemple un ls, mais tu y met l’exécutable que tu veux comme un fichier C compiler
          //if (err) throw err; //Decommenter pour arreter le server en cas derreur sur lexecution de la commande et lever cette erreure
          if (stderr) console.warn(stderr); //On affiche lerreur eventuelle
          console.log(stdout); //On affiche la sortie standard du programme
          res.redirect('/index'); //Redirection vers la page view
        });
    } else {
        res.redirect('/index');
    }
})
*/


/* On redirige vers la page d'accueille si la page demandée n'est pas trouvée */
.use(function(req, res, next){
    res.redirect('/index');
})

var server = http.createServer(app);

// On se connecte au broker
const client = mqtt.connect('mqtt://10.0.0.59/');
console.log('Le serveur se connecte au broker...');


client.on('connect', function () {
  //On s'abonne à toux les flux
  client.subscribe('tables/#');
});

client.on('message', function (topic, txt) {
  console.log('Nouveau msg MQTT, topic : ', topic);

  //Traitement du flux
  if (topic.indexOf('tables/cmd_') > -1 && topic.indexOf('/nouvelleCmd') < 0) {
    //On informe tous les client Webs avec socket.io
    io.sockets.emit('newCmd', topic, txt.toString());
  } else if (topic.indexOf('tables/etat_') > -1) {
    io.sockets.emit('updateEtat', topic, txt.toString());
  } else if (topic.indexOf('tables/set_alarmes/') > -1) {
    io.sockets.emit('updateEtatSetAlarmes', topic, txt.toString());
  } else if (topic.indexOf('tables/alarmes/alarmeAdd') > -1) {
    io.sockets.emit('nouvelleAlarme', txt.toString());
  } else if (topic.indexOf('tables/journal/logAdd') > -1) {
    io.sockets.emit('nouvelleLog', txt.toString());
  } else if (topic.indexOf('tables/journal/updateAcquittement') > -1) {
    io.sockets.emit('updateLogAcquittement', txt.toString());
  }
});

var io = require('socket.io').listen(server);

io.sockets.on('connection', function (socket, message) {

    socket.on('nouvelle_connexion', function(ID) {
        socket.ID = ID;
        console.log(socket.ID, 'vient de se connecter');
    });

    socket.on('nouvelle_cmd_ram', function(cmd, p1, p2) {
      client.publish('tables/cmd_ram/nouvelleCmd/',
        cmd + '/' + p1 + '/' + p2 + '/' + socket.ID);
    });

    socket.on('nouvelle_cmd_melangeur', function(cmd, p1, p2, p3) {
      client.publish('tables/cmd_melangeur/nouvelleCmd/',
        cmd + '/' + p1 + '/' + p2 + '/' + p3 + '/' + socket.ID);
    });

    socket.on('nouvelle_valeur_set_alarmes', function(col, val) {
      client.publish('tables/set_alarmes/' + col, val);
    });

    socket.on('acquitterAlarme', function(LogID, msg) {
      client.publish('tables/journal/requestAcquittement',
        LogID + '/' + msg);
    });

});


var port = Number(process.env.PORT || 3000);

server.listen(port);

app.listen(8080);


function SQL(requete, data, utiliser_resultats) { // Exeute une requete SQL pour MariaDB
    console.log(requete);
 
    var c = new ClientDB({
        host: '127.0.0.1',
        user: 'root',
        password: '',
        db: 'RAM'
    });
       
    c.query(requete, data,
        function(err, rows) {
        if (err)
            throw err;
        utiliser_resultats(rows)
    });
       
    c.end();
}