################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DbSql.cpp \
../src/MQTT.cpp \
../src/alarme.cpp \
../src/clavier.cpp \
../src/control.cpp \
../src/main.cpp \
../src/mutex.cpp \
../src/mutexAlloc.cpp \
../src/pid.cpp \
../src/queue.cpp \
../src/ram.cpp \
../src/screen.cpp \
../src/sem.cpp \
../src/sql.cpp \
../src/temps.cpp 

OBJS += \
./src/DbSql.o \
./src/MQTT.o \
./src/alarme.o \
./src/clavier.o \
./src/control.o \
./src/main.o \
./src/mutex.o \
./src/mutexAlloc.o \
./src/pid.o \
./src/queue.o \
./src/ram.o \
./src/screen.o \
./src/sem.o \
./src/sql.o \
./src/temps.o 

CPP_DEPS += \
./src/DbSql.d \
./src/MQTT.d \
./src/alarme.d \
./src/clavier.d \
./src/control.d \
./src/main.d \
./src/mutex.d \
./src/mutexAlloc.d \
./src/pid.d \
./src/queue.d \
./src/ram.d \
./src/screen.d \
./src/sem.d \
./src/sql.d \
./src/temps.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/include/mysql++ -I/usr/include/mysql -O0 -g3 -Wall -Werror -c -fmessage-length=0 -D_GNU_SOURCE -D_REENTRANT $(mysql_config --cflags) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


