#include "alarme.hpp"
#include "screen.hpp"

TAlarme *alarme = NULL;

TAlarme::TAlarme(const char *name,sharedData_t *shared,int policy,int priority,destruction_t destruction,int noCpu) :
                                                            TThread(name,shared,policy,priority,destruction,noCpu)
    {
    partageRam = ram->getPartageRam();

    // init statusAlarme
    memset(&statusAlarme,0,sizeof statusAlarme);

    screen->dispStr(1,6,"Alarme :");
    }

TAlarme::~TAlarme()
    {
    }

void TAlarme::task(void)
    {
    // variable locale
    char strCar[2] = {'-','\0'};
    //char bufStr[50];
    alarmes_t statusAlarme;
    TRam::ram_t statusRam;

    // synchronisation démarrage tâche
    signalStart();

    while(1)
	{
	// attendre acquisition complété ou débordement détecté
	partageRam->synAlarme.take();

	//getStatusAlarme(&statusAlarme);
	ram->getStatusRam(&statusRam);

	if( (statusRam.debordementGrosBassin == 0) || (statusRam.debordementPetitBassin == 0) )
	    {
	    ram->init();

	    if(statusRam.debordementGrosBassin)
		statusAlarme.overflowGrosBassin = 1;
	    if(statusRam.debordementPetitBassin)
		statusAlarme.overflowPetitBassin = 1;
	    }

	setStatusAlarme(&statusAlarme);

	//traitement
	if(strCar[0] == '|')
		strCar[0] = '-';
	else
		strCar[0] = '|';
	screen->dispStr(11,6,strCar);

	if(thread.destruction == DESTRUCTION_SYNCHRONE)
	    {
	    // point de destruction
	    pthread_testcancel();
	    }
	}
    }

// Partage
void TAlarme::setStatusAlarme(alarmes_t *status)
    {
    lock.take();
    statusAlarme = *status;
    lock.release();
    }

void TAlarme::getStatusAlarme(alarmes_t *status)
    {
    lock.take();
    *status = statusAlarme;
    lock.release();
    }

