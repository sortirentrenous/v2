#ifndef FILTRE_FENETRE_GLISSANTE_HPP
#define FILTRE_FENETRE_GLISSANTE_HPP

class TFiltreFenetreGlissante
    {
    private:
	double *tabFiltre;
	int W,N;
    public:
	TFiltreFenetreGlissante(int fenetre = 3);
	~TFiltreFenetreGlissante();

	double filtre(double val);
    };

#endif // FILTRE_FENETRE_GLISSANTE_HPP
