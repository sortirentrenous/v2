#ifndef ALARME_HPP
#define ALARME_HPP

#include "thread.hpp"
#include "ram.hpp"

class TAlarme : public TThread
    {
    public:
	struct setAlarmes_t
	    {
	    short niveauMaxGrosBassin;
	    short niveauMinGrosBassin;
	    short niveauMaxPetitBassin;
	    short niveauMinPetitBassin;
	    short tempMaxGrosBassin;
	    short tempMinGrosBassin;
	    short tempMaxPetitBassin;
	    short tempMinPetitBassin;
	    };

	struct alarmes_t
	    {
	    unsigned char overflowGrosBassin;
	    unsigned char overflowPetitBassin;
	    unsigned char niveauMaxGrosBassin;
	    unsigned char niveauMinGrosBassin;
	    unsigned char niveauMaxPetitBassin;
	    unsigned char niveauMinPetitBassin;
	    unsigned char tempMaxPetitBassin;
	    unsigned char tempMinPetitBassin;
	    };

    private:
	alarmes_t statusAlarme;
	TMutex lock;
	TRam::partageRam_t *partageRam;

    public:
	TAlarme(const char *name,sharedData_t *shared,int policy,int priority,destruction_t destruction,int noCpu = -1);
	~TAlarme();

	// tâche control
	void task(void);

	// Partage
	void setStatusAlarme(alarmes_t *status);
	void getStatusAlarme(alarmes_t *status);
    };

extern TAlarme *alarme;

#endif //ALARME_HPP
