#ifndef TTASK4_HPP
#define TTASK4_HPP



#include "thread.hpp"
#include "ram.hpp"

class TTask4 : public TThread
    {
    private:
	TRam::ram_t statusRam;
    public:
	TTask4(const char *name,sharedData_t *shared,int policy,int priority,destruction_t destruction,int cpu = -1);
	~TTask4();

	void task(void);
    };

#endif //TTASK1_HPP
