#ifndef PID_H_
#define PID_H_

#include <stdlib.h>

class TPid
    {
    public:
	enum outputPid_t
	    {
	    CENT_TO_MOINS_CENT,
	    ZERO_TO_CENT
	    };
    private:
	double Kp;
	double Ki;
	double Kd;
	double Null;
	double Error;
	int Gap;
	int PourcentErrorIEffectif;
	outputPid_t TypeOutput;
	double IErrCumul;
	double Output;
	double PrevOutput;
    public:
	TPid(double kp,double ki,double kd,double null,int gap,int pourcentErrorIEffectif,outputPid_t typeOutput);
	~TPid();

	void reset(void);
	void init(double kp,double ki,double kd,double null,int gap,int pourcentErrorIEffectif,outputPid_t typeOutput);
	void update(double consigne,double valeurMesuree);
	double getOutput(void);
	double getError(void);
	void clearIErrCumul(void);
    };

#endif // PID_H_
