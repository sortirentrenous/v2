/*
 * DbSql.cpp
 *
 * Drapeau pour l'environnement de développement
 *
 * Drapeaux du compilateur (CFLAGS) : $(mysql_config --cflags) -I/usr/include/mysql
 * Drapeaux de l'editeur de liens (LDFLAGS) : $(mysql_config --libs) -lmysqlpp
 * Drapeaux du compilateur (CXXFLAGS) : $(mysql_config --cflags) -I/usr/include/mysql -I/usr/include/mysql++
 *
 */
/*
#include "DbSql.h"

void updateColumn(Connection *con,const char *table,const char *column, const char *value,
                                             const char *whereColumn,const char *whereValue)
	{
	if(con->ping())
		{
  		char command[50 + strlen(table) + strlen(column) + 
                        strlen(value) + strlen(whereColumn) + strlen(whereValue)];
		Query query = con->query();
		*/

		/*
  		  command = "update " + table + " set " + column + " = '" + value;
  		  command += "' where " + whereColumn + " = '" + whereValue + "'";
		*/
/*
		strcpy(command,"update ");
		strcat(command,table);
		strcat(command," set ");
		strcat(command,column);
		strcat(command," = '");
		strcat(command,value);
		strcat(command,"' where ");
		strcat(command,whereColumn);
		strcat(command," = '");
		strcat(command,whereValue);
		strcat(command,"'");
		query << command;
		query.exec();
		}
	}

void updateColumn(Connection *con,const char *table,
				  const char *column,
				  const char *value,
                                  const char *whereColumn1,
                                  const char *whereValue1,
                                  const char *whereColumn2,
                                  const char *whereValue2)
	{
	if(con->ping())
		{
  		char command[50 + strlen(table) + strlen(column) + 
                        strlen(value) + strlen(whereColumn1) + strlen(whereValue1) +
  							        strlen(whereColumn2) + strlen(whereValue2)];
		Query query = con->query();
*/
		/*
  		  command = "update " + table + " set " + column + " = '" + value;
  		  command += "' where " + whereColumn + " = '" + whereValue + "'";
		*/
/*
		strcpy(command,"update ");
		strcat(command,table);
		strcat(command," set ");
		strcat(command,column);
		strcat(command," = '");
		strcat(command,value);
		strcat(command,"' where ");
		strcat(command,whereColumn1);
		strcat(command," = '");
		strcat(command,whereValue1);
		strcat(command,"' and ");
		strcat(command,whereColumn2);
		strcat(command," = '");
		strcat(command,whereValue2);
		strcat(command,"'");
		query << command;
		query.exec();
		}
	}

*/
/*
void updateColumn(Connection *con,const string &table,const string &column, const string &value,
                                          const string &whereColumn,const string &whereValue)

	{
	if(con->ping())
		{
  		string command;
		Query query = con->query();

  		command = "update " + table + " set " + column + " = " + "'" + value + "'";
  		command += " where " + whereColumn + " = " + "'" + whereValue + "'";
		query << command;
		query.exec();
		}
	}

void updateColumn(Connection *con,string table,string column1,string value1,
                                                   string column2,string value2,
                                                   string whereColumn,string whereValue)
	{
	if(con->ping())
		{
  		string command;
		Query query = con->query();

  		command = "update " + table + " set " + column1 + " = " + value1;
		command += ", " + column2 + " = " + value2;
  		command += " where " + whereColumn + " = " + whereValue;
		query << command;
		query.exec();
		}
	}

void updateColumn(Connection *con,string table,string column1,string value1,
                                                   string column2,string value2,
                                                   string column3,string value3,
                                                   string whereColumn,string whereValue)
	{
	if(con->ping())
		{
  		string command;
		Query query = con->query();

  		command = "update " + table + " set " + column1 + " = " + value1;
		command += ", " + column2 + " = " + value2;
		command += ", " + column3 + " = " + value3;
  		command += " where " + whereColumn + " = " + whereValue;
		query << command;
		query.exec();
		}
	}

void updateColumn(Connection *con,string table,string column1,string value1,
                                                   string column2,string value2,
                                                   string column3,string value3,
                                                   string column4,string value4,
                                                   string whereColumn,string whereValue)
	{
	if(con->ping())
		{
  		string command;
		Query query = con->query();

  		command = "update " + table + " set " + column1 + " = " + value1;
		command += ", " + column2 + " = " + value2;
		command += ", " + column3 + " = " + value3;
		command += ", " + column4 + " = " + value4;
  		command += " where " + whereColumn + " = " + whereValue;
		query << command;
		query.exec();
		}
	}


int unescape_string(string escString,void *unescBuf)
	{
	char *pBuf = (char *)unescBuf;
	int i = 0;

	for(int c = 0;c < (int)escString.length();c++)
		{
		if(escString.at(c) != '\\')
			{
			pBuf[i++] = escString.at(c);
			}
		else
			{
			switch(escString.at(c + 1))
				{
				case '0':
					pBuf[i++] = 0x00;
					c++;
					break;
				case '\'':
					pBuf[i++] = 0x27;
					c++;
					break;
				case '\"':
					pBuf[i++] = 0x22;
					c++;
					break;
				case 'b':
					pBuf[i++] = 0x08;
					c++;
					break;
				case 'n':
					pBuf[i++] = 0x0a;
					c++;
					break;
				case 'r':
					pBuf[i++] = 0x0d;
					c++;
					break;
				case 't':
					pBuf[i++] = 0x09;
					c++;
					break;
				case 'Z':
					pBuf[i++] = 0x1a;
					c++;
					break;
				case '\\':
					pBuf[i++] = 0x5C;
					c++;
					break;
				}
			}
		}

	return i;
	}
*/
