#include "sql.hpp"
#include "screen.hpp"
#include "temps.hpp"
/*
TSql *sql = NULL;

TSql::TSql(const char *name,sharedData_t *shared,int policy,int priority,destruction_t destruction,int noCpu) :
                                              TThread(name,shared,policy,priority,destruction,noCpu), conSql(false)
    {
    screen->dispStr(1,7,"Sql :");
    }

TSql::~TSql()
    {
    conSql.disconnect();
    }

void TSql::task(void)
    {
    // variable locale
    char strCar[2] = {'-','\0'};
    bool isCmd = false;
    int nbRow;
    int cmdID,cmdLog,p1 = 0,p2 = 0;
    ramCmd_t cmd;

    // synchronisation démarrage tâche
    signalStart();

    conSql.connect(
		    DATABASE_NAME,   // database name
		    HOST_NAME,       // host name
		    USER_NAME,       // user name
		    PASSWORD) ;      // password

    sleep(3);
    if(conSql.connected())
	screen->dispStr(30,7,"SQL Connexion OK    ");
    //else
	//screen->dispStr(30,7,"SQL Connexion ERREUR");

    while(1)
	{
	//traitement
	if(conSql.ping())
	    {
	    Query query = conSql.query();
	    Query queryCmd = conSql.query();
	    StoreQueryResult res;

	    if(strCar[0] == '|')
		    strCar[0] = '-';
	    else
		    strCar[0] = '|';
	    screen->dispStr(7,7,strCar);

	    // commande

	    query.reset();
	    query << "select * from cmd_ram order by cmd_id";
	    res = query.store();
	    nbRow = res.num_rows();
	    if(nbRow)
		{
		isCmd = true;

		cmdID  = atoi(((string)res[0]["cmd_id"]).c_str());
		cmdLog = atoi(((string)res[0]["log_id"]).c_str());
		cmd    = (ramCmd_t)atoi(((string)res[0]["cmd"]).c_str());
		p1     = atoi(((string)res[0]["p1"]).c_str());
		p2     = atoi(((string)res[0]["p2"]).c_str());
		}

	    if(isCmd)
		{
		isCmd = false;

		if(executeCmd(cmd,p1,p2))
		    {
		    // mise à jour table journal
		    queryCmd.reset();
		    queryCmd << "update journal set DoneTime = '" << temps->now() << "' where LogID = " << cmdLog;
		    queryCmd.exec();
		    }
		else // commande erronée
		    {
		    // inscrire dans la table alarmes
		    queryCmd.reset();
		    queryCmd << "insert into alarmes values(0," << ALR_CMD_ERR << ",0,'" << temps->now() << "')";
		    queryCmd.exec();
		    }

		// détruire commande
		queryCmd.reset();
		queryCmd << "delete from cmd_ram where cmd_id = " << cmdID;
		queryCmd.exec();
		}

	    // mise à jour table etat_ram
	    ram->getStatusRam(&statusRam);
	    queryCmd.reset();
	    queryCmd << "update etat_ram set Mode=" << statusRam.mode
		     << ",ConsNivGB=" << (int)statusRam.consigneNiveauGrosBassin
		     << ",ConsNivPB=" << (int)statusRam.consigneNiveauPetitBassin
		     << ",ConsTmpPB=" << (int)statusRam.consigneTemperaturePetitBassin
		     << ",NivGB=" << statusRam.niveauGrosBassin
		     << ",NivPB=" << statusRam.niveauPetitBassin
		     << ",TmpGB=" << statusRam.temperatureGrosBassin
		     << ",TmpPB=" << statusRam.temperaturePetitBassin
		     << ",ValveGB=" << statusRam.valveGrosBassin
		     << ",ValvePB=" << statusRam.valvePetitBassin
		     << ",ValveEC=" << statusRam.valveEauChaude
		     << ",ValveEF=" << statusRam.valveEauFroide
		     << ",EntreeEC=" << statusRam.eauChaude
		     << ",EntreeEF=" << statusRam.eauFroide
		     << ",Pompe=" << statusRam.pompe;

            //screen->dispStr(1,40,queryCmd.str().c_str());
	    queryCmd.exec();

	    // vérification alarmes
	    alarme->getStatusAlarme(&statusAlarme);

	    if(statusAlarme.overflowGrosBassin == 1)
		{
		if(checkIfGenereAlarme(ALR_GB_OVF) == false) //alarme déja générée?
		    {
		    genereAlarme(ALR_GB_OVF);
		    statusAlarme.overflowGrosBassin = 0; // généré //tmp
		    alarme->setStatusAlarme(&statusAlarme);
		    }
		}
	    if(statusAlarme.overflowPetitBassin == 1)
		{
		if(checkIfGenereAlarme(ALR_PB_OVF) == false) //alarme déja générée?
		    {
		    genereAlarme(ALR_PB_OVF);
		    statusAlarme.overflowPetitBassin = 0; // généré // tmp
		    alarme->setStatusAlarme(&statusAlarme);
		    }
		}

	    } // if(con.ping())
	else
	    {
	    conSql.connect(
			    DATABASE_NAME,   // database name
			    HOST_NAME,       // host name
			    USER_NAME,       // user name
			    PASSWORD);       // password

	    if(conSql.connected())
		screen->dispStr(30,7,"SQL Connexion OK    ");
	    else
		screen->dispStr(30,7,"SQL Connexion ERREUR");
	    }

	if(thread.destruction == DESTRUCTION_SYNCHRONE)
	    {
	    // point de destruction
	    pthread_testcancel();
	    }

	usleep(500000);   // 500 ms
	}
    }

int TSql::executeCmd(ramCmd_t cmd,int p1,int p2)
    {
    int retour = 1;

    switch(cmd)
	{
	case RAM_MODE :
	    ram->setMode(p1);
	    break;
	case RAM_NIV_BASSIN :
	    if(ram->getMode() == AUTO)
		{
		if((ram_bassin_t)p1 == GROS_BASSIN)
		    ram->setConsigneNiveauGrosBassin(p2);
		else if((ram_bassin_t)p1 == PETIT_BASSIN)
		    ram->setConsigneNiveauPetitBassin(p2);
		else
		    retour = 0;
		}
	    else
		retour = 0;
	    break;
	case RAM_TEMP_BASSIN :
	    if((ram_bassin_t)p1 == PETIT_BASSIN)
		ram->setConsigneTemperaturePetitBassin(p2);
	    else
		retour = 0;
	    break;
	case RAM_VALVE :
	    if(ram->getMode() == MANUEL)
		{
		switch(p1)
		    {
		    case VALVE_GROS_BASSIN :
			ram->setValveGrosBassin(p2);
			break;
		    case VALVE_PETIT_BASSIN :
			ram->setValvePetitBassin(p2);
			break;
		    case VALVE_EAU_CHAUDE :
			ram->setValveEauChaude(p2);
			break;
		    case VALVE_EAU_FROIDE :
			ram->setValveEauFroide(p2);
			break;
		    case VALVE_ENTREE_CHAUDE :
			ram->setEauChaude(p2);
			break;
		    case VALVE_ENTREE_FROIDE :
			ram->setEauFroide(p2);
			break;
		    default:
			retour = 0;
			break;
		    }
		}
	    else
		retour = 0;
	    break;
	case RAM_POMPE :
	    ram->setPompe(p1);
	    break;
	default:
	    retour = 0;
	    break;
	}

    return retour;
    }

void TSql::genereAlarme(alarme_t type)
	{
	int val = 0;
	Query queryCmd = conSql.query();

	switch((int)type)
	    {
	    case ALR_GB_OVF:
	    case ALR_PB_OVF:
		val = 1;
		break;
	    case ALR_GB_NIV_MAX:
		val = statusRam.niveauGrosBassin;
		break;
	    case ALR_GB_NIV_MIN:
		val = statusRam.niveauGrosBassin;
		break;
	    case ALR_PB_NIV_MAX:
		val = statusRam.niveauPetitBassin;
		break;
	    case ALR_PB_NIV_MIN:
		val = statusRam.niveauPetitBassin;
		break;
	    case ALR_PB_TMP_MAX:
		val = statusRam.temperaturePetitBassin;
		break;
	    case ALR_PB_TMP_MIN:
		val = statusRam.temperaturePetitBassin;
		break;
	    }

	// inscrire dans la table alarmes
	queryCmd << "insert into alarmes values(0," << (int)type << "," << val << ",'" << temps->now() << "')";
	queryCmd.exec();
	}

bool TSql::checkIfGenereAlarme(alarme_t type)
	{
	bool retour = false;
	Query query = conSql.query();
	StoreQueryResult res;

	// recherche dans la table alarmes
	query << "select * from alarmes where type = " << type;
	res = query.store();
	if(res.num_rows())
	    {
	    retour = true;
	    }

	return retour;
	}
*/
