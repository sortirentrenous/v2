#include "sem.hpp"

TSemaphore::TSemaphore(unsigned int valeur)
    {
    sem_init(&sem,0,valeur);
    }

TSemaphore::~TSemaphore()
    {
    sem_destroy(&sem);
    }

void TSemaphore::take(void)
    {
    sem_wait(&sem);
    }

void TSemaphore::release(void)
    {
    sem_post(&sem);
    }
