#ifndef SEM_HPP
#define SEM_HPP

#include <semaphore.h>

class TSemaphore
    {
    private:
	sem_t sem;
    public:
	TSemaphore(unsigned int valeur = 1);
	~TSemaphore();

	void take(void);
	void release(void);
    };

#endif // SEM_HPP
