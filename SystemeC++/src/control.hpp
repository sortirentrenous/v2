#ifndef CONTROL_HPP
#define CONTROL_HPP

#include "thread.hpp"
#include "ram.hpp"
#include "pid.hpp"

class TControl : public TThread
    {
    private:
	TRam::partageRam_t *partageRam;

	TPid pidGrosBassin;
	TPid pidPetitBassin;
	TPid pidTempPetitBassin;
    public:
	TControl(const char *name,sharedData_t *shared,int policy,int priority,destruction_t destruction,int noCpu = -1);
	~TControl();

	// tâche control
	void task(void);
    };

extern TControl *control;

#endif //CONTROL_HPP
