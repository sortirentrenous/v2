#ifndef SQL_HPP
#define SQL_HPP

#include "thread.hpp"
#include "ram.hpp"
#include "alarme.hpp"
#include "DbSql.h"

class TSql : public TThread
    {
    public:
	enum ramCmd_t
	    {
	    RAM_NIV_BASSIN,
	    RAM_TEMP_BASSIN,
	    RAM_VALVE,
	    RAM_POMPE,
	    RAM_MODE
	    };

	enum mode_operation_t
	    {
	    AUTO,
	    MANUEL,
	    CALIBRATION,
	    NONE_MODE
	    };

	enum ram_bassin_t
	    {
	    PETIT_BASSIN,
	    GROS_BASSIN
	    };

	enum ram_valve_t
	    {
	    VALVE_GROS_BASSIN,
	    VALVE_PETIT_BASSIN,
	    VALVE_EAU_CHAUDE,
	    VALVE_ENTREE_CHAUDE,
	    VALVE_ENTREE_FROIDE,
	    VALVE_EAU_FROIDE
	    };

	enum alarme_t
	    {
	    ALR_GB_OVF,
	    ALR_PB_OVF,
	    ALR_GB_NIV_MAX,
	    ALR_GB_NIV_MIN,
	    ALR_PB_NIV_MAX,
	    ALR_PB_NIV_MIN,
	    ALR_GB_TMP_MAX,
	    ALR_GB_TMP_MIN,
	    ALR_PB_TMP_MAX,
	    ALR_PB_TMP_MIN,
	    ALR_COM_ERR_BAL,
	    ALR_COM_ERR_MEL,
	    ALR_COM_ERR_CAM,
	    ALR_CMD_ERR
	    };

    private:
	TRam::ram_t statusRam;
	TAlarme::alarmes_t statusAlarme;
	//Connection conSql;

	int executeCmd(ramCmd_t cmd,int p1,int p2);
	void genereAlarme(alarme_t type);
	bool checkIfGenereAlarme(alarme_t type);

    public:
	TSql(const char *name,sharedData_t *shared,int policy,int priority,destruction_t destruction,int noCpu = -1);
	~TSql();

	// tâche Sql
	void task(void);
    };

extern TSql *sql;

#endif //SQL_HPP
