#include "mutex.hpp"

TMutex::TMutex(void)
    {
    pthread_mutexattr_t mutexattr;

    // Initialisation attribut
    pthread_mutexattr_init(&mutexattr);
    pthread_mutexattr_setprotocol(&mutexattr,PTHREAD_PRIO_INHERIT); //protection inversion priorité

    pthread_mutex_init(&mutex,&mutexattr);

    // Libère attribut
    pthread_mutexattr_destroy(&mutexattr);
    }

TMutex::~TMutex()
    {
    pthread_mutex_destroy(&mutex);
    }

void TMutex::take(void)
    {
    pthread_mutex_lock(&mutex);
    }

void TMutex::release(void)
    {
    pthread_mutex_unlock(&mutex);
    }
