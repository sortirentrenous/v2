#ifndef CLAVIER_HPP
#define CLAVIER_HPP

#include <stdio.h>
#include <stdlib.h>
#include <termio.h>

class TClavier
    {
    private:
	struct termios oldTermios;
	const int timeoutMs;		// temps d'attende d'un caractère au clavier

	int reconfigureTerminal(void);
	int restaureTerminal(void);
    public:
	TClavier(void);
	~TClavier();

	int kbhit(void);
	int getch(void);
    };

extern TClavier *clavier;

#endif // CLAVIER_HPP
