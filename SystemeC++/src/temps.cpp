#include "temps.hpp"

TTemps *temps = NULL;

TTemps::TTemps(void)
    {
    memset(&tDebut,0,sizeof(struct timeval));
    memset(&tFin,0,sizeof(struct timeval));
    }

char *TTemps::now(void)
    {
    lock.take();

    time(&temps);
    localtime_r(&temps,&tmTemps);

    strftime(strTemps,sizeof strTemps,"%F %T",&tmTemps);

    lock.release();

    return strTemps;
    }

void TTemps::startMesure(void)
    {
    gettimeofday(&tDebut,NULL);
    }

void TTemps::stopMesure(void)
    {
    gettimeofday(&tFin,NULL);
    }

double TTemps::mesure_us(void)
    {
    return (((tFin.tv_sec - tDebut.tv_sec) * 1000000) + (tFin.tv_usec - tDebut.tv_usec));
    }

