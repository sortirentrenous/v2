#include "pid.hpp"

TPid::TPid(double kp,double ki,double kd,double null,int gap,int pourcentErrorIEffectif,outputPid_t typeOutput)
    {
    init(kp,ki,kd,null,gap,pourcentErrorIEffectif,typeOutput);
    }

TPid::~TPid()
    {

    }

void TPid::reset(void)
    {
    Kp = 0;
    Ki = 0;
    Kd = 0;
    Null = 0;
    Error = 0;
    Gap = 0;
    PourcentErrorIEffectif = 0;
    IErrCumul = 0;
    Output = 0;
    PrevOutput = 0;
    }

void TPid::init(double kp,double ki,double kd,double null,int gap,int pourcentErrorIEffectif,outputPid_t typeOutput)
    {
    Error = 0;
    IErrCumul = 0;
    Output = 0;
    PrevOutput = 0;

    Kp = kp;
    Ki = ki;
    Kd = kd;
    Null = null;
    Gap = gap;
    PourcentErrorIEffectif = pourcentErrorIEffectif;
    TypeOutput = typeOutput;
    }

void TPid::update(double consigne,double valeurMesuree)
    {
    int limitBasseCorrectionIntegral;
    double errKp,errKi,output;

    //output = Output;
    Error = consigne - valeurMesuree;

    // proportionnel
    errKp = Error * Kp;
    output = errKp + Null;

    // intégral
    if( (Ki > 0) && (abs(Error) <= PourcentErrorIEffectif) )
      output = output + IErrCumul;
    else
      IErrCumul = 0;

    if(abs(output - PrevOutput) >= Gap)
      {
      PrevOutput = output;

      if(output > 100)
	 {
	 output = 100;
	 IErrCumul = 0;
	 }
      else if(output < 0)
	{
	if(TypeOutput == ZERO_TO_CENT)
	    {
	    output = 0;
	    //IErrCumul = 0;
	    }
	else if(output < -100)
	    {
	    output = -100;
	    //IErrCumul = 0;
	    }
	}

      Output = output;
      }

    if( (Ki > 0) && (abs(Error) <= PourcentErrorIEffectif) && (abs(Error) != 0) )
	{
	limitBasseCorrectionIntegral = (TypeOutput == ZERO_TO_CENT)? 0 : -100;

	// limiter les extrêmes de la correction intégrale
	if( ( (output < 100) || ( (output >= 100) && (Error < 0) ) )   &&
	    ( (output > limitBasseCorrectionIntegral)
	       || ( (output <= limitBasseCorrectionIntegral)  && (Error > 0) ) ) )
	     {
	     errKi = Error * Ki;
	     IErrCumul += errKi;
	     }
	//else
	    //IErrCumul = 0;
      }
    }

double TPid::getOutput(void)
    {
    return Output;
    }

double TPid::getError(void)
    {
    return Error;
    }

void TPid::clearIErrCumul(void)
    {
    IErrCumul = 0;
    }

