//============================================================================
// Name        : RAM
// Author      : SG
// Version     :
// Copyright   : Your copyright notice
// Description : Controleur Panneau RAM
//============================================================================

#include <string>
using namespace std;

#include "clavier.hpp"
#include "screen.hpp"
#include "temps.hpp"
#include "alarme.hpp"
#include "control.hpp"
#include "ram.hpp"
#include "MQTT.hpp"
#include "version.hpp"

void effaceMenu(void);
void menuAuto(TRam::ram_t *status);
void menuManuel(TRam::ram_t *status);

int main(int argc, char *argv[])
    {
    char car;
    char bufStr[50];
    bool mode = 1,pompe = 0,eauChaude = 0,eauFroide = 0;
    int noCpu = 0;
    int valveEauChaude = 0,valveEauFroide = 0,valveGrosBassin = 0,valvePetitBassin = 0;
    int consigneNiveauGrosBassin = 0,consigneNiveauPetitBassin = 0,consigneTemperaturePetitBassin = 0;
    int niveauGrosBassin = -1,niveauPetitBassin = -1,temperatureGrosBassin = -1,temperaturePetitBassin = -1;
    int valveEC = -1,valveEF = -1,valveGB = -1,valvePB = -1,overflowGB = -1,overflowPB = -1;

    // Initialisation task Principal
    TThread::initTaskMain(SCHED_FIFO,noCpu);

    // Création Clavier,console et horloge
    clavier = new TClavier();
    temps   = new TTemps();
    screen  = new TScreen();
    screen->start();

    screen->dispStr(1,1,"RAM Init...");

    // Création tâches
    noCpu = 0;
    ram     = new TRam("Ram",NULL,SCHED_RR,50,TThread::DESTRUCTION_ASYNCHRONE,noCpu);
    alarme  = new TAlarme("Alarme",NULL,SCHED_RR,80,TThread::DESTRUCTION_SYNCHRONE,noCpu);
    control = new TControl("Control",NULL,SCHED_RR,70,TThread::DESTRUCTION_SYNCHRONE,noCpu);
    noCpu = 0;
    TTask4 *task4 = new TTask4("MQTT",NULL,SCHED_RR,40,TThread::DESTRUCTION_SYNCHRONE,noCpu);

    // Démarrage tâches
    //alarme->start();
    //control->start();
    ram->start();
    task4->start();

    // attendre que les taches soit demarrees
    sleep(1);

    ram->init();

    TRam::ram_t statusRam;
    ram->getStatusRam(&statusRam);

    // Traitement tâche principale
    string message;
    message = "RAM (Version : ";
    message += VERSION;
    message += "   ";
    message += DATE;
    message += "   ";
    message += AUTEUR;
    message += ")";
    screen->dispStr(1,1,message.c_str());

    screen->dispStr(1,10,"1 Mode  (manuel)");
    screen->dispStr(1,11,"2 Pompe (off)");
    screen->dispStr(1,12,"3 Arrêt");
    menuManuel(&statusRam);
    screen->dispStr(1,22,"Niveau");
    screen->dispStr(1,23,"GB :");
    screen->dispStr(1,24,"PB :");
    screen->dispStr(25,22,"Temperature");
    screen->dispStr(25,23,"GB :");
    screen->dispStr(25,24,"PB :");
    screen->dispStr(55,22,"Valve");
    screen->dispStr(55,23,"GB :");
    screen->dispStr(55,24,"PB :");
    screen->dispStr(55,25,"EC :");
    screen->dispStr(55,26,"EF :");
    screen->dispStr(75,22,"Débordement");
    screen->dispStr(75,23,"GB :");
    screen->dispStr(75,24,"PB :");

    do
      {
      // Traitement
      ram->getStatusRam(&statusRam);

      // affichage
      if(mode != statusRam.mode)
	  {
	  effaceMenu();
	  mode = statusRam.mode;
	  if(statusRam.mode == 0)
	      {
	      screen->dispStr(9,10,"(auto)  ");
	      menuAuto(&statusRam);
	      }
	  else
	      {
	      screen->dispStr(9,10,"(manuel)");
	      menuManuel(&statusRam);
	      }
	  }
      if(pompe != statusRam.pompe)
	  {
	  pompe = statusRam.pompe;
	  if(statusRam.pompe)
	      {
	      screen->dispStr(9,11,"(on) ");
	      }
	  else
	      {
	      screen->dispStr(9,11,"(off)");
	      }
	  }

      if(niveauGrosBassin != statusRam.niveauGrosBassin)
	  {
	  niveauGrosBassin = statusRam.niveauGrosBassin;
	  sprintf(bufStr,"%03d %%",(int)statusRam.niveauGrosBassin);
	  screen->dispStr(6,23,bufStr);
	  }
      if(niveauPetitBassin != statusRam.niveauPetitBassin)
	  {
	  niveauPetitBassin = statusRam.niveauPetitBassin;
	  sprintf(bufStr,"%03d %%",(int)statusRam.niveauPetitBassin);
	  screen->dispStr(6,24,bufStr);
	  }

      if(temperatureGrosBassin != statusRam.temperatureGrosBassin)
	  {
	  temperatureGrosBassin = statusRam.temperatureGrosBassin;
	  sprintf(bufStr,"%05.1lf Cel",statusRam.temperatureGrosBassin);
	  screen->dispStr(30,23,bufStr);
	  }
      if(temperaturePetitBassin != statusRam.temperaturePetitBassin)
	  {
	  temperaturePetitBassin = statusRam.temperaturePetitBassin;
	  sprintf(bufStr,"%05.1lf Cel",statusRam.temperaturePetitBassin);
	  screen->dispStr(30,24,bufStr);
	  }

      if(valveGB != statusRam.valveGrosBassin)
	  {
	  valveGB = statusRam.valveGrosBassin;
	  sprintf(bufStr,"%03d %%",(int)statusRam.valveGrosBassin);
	  screen->dispStr(60,23,bufStr);
	  }
      if(valvePB != statusRam.valvePetitBassin)
	  {
	  valvePB = statusRam.valvePetitBassin;
	  sprintf(bufStr,"%03d %%",(int)statusRam.valvePetitBassin);
	  screen->dispStr(60,24,bufStr);
	  }
      if(valveEC != statusRam.valveEauChaude)
	  {
	  valveEC = statusRam.valveEauChaude;
	  sprintf(bufStr,"%03d %%",(int)statusRam.valveEauChaude);
	  screen->dispStr(60,25,bufStr);
	  }
      if(valveEF != statusRam.valveEauFroide)
	  {
	  valveEF = statusRam.valveEauFroide;
	  sprintf(bufStr,"%03d %%",(int)statusRam.valveEauFroide);
	  screen->dispStr(60,26,bufStr);
	  }

      if(overflowGB != statusRam.debordementGrosBassin)
	  {
	  overflowGB = statusRam.debordementGrosBassin;
	  sprintf(bufStr,"%d",statusRam.debordementGrosBassin);
	  screen->dispStr(81,23,bufStr);
	  }
      if(overflowPB != statusRam.debordementPetitBassin)
	  {
	  overflowPB = statusRam.debordementPetitBassin;
	  sprintf(bufStr,"%d",statusRam.debordementPetitBassin);
	  screen->dispStr(81,24,bufStr);
	  }

      if(mode == 1) // manuel
	  {
	  if(eauFroide != statusRam.eauFroide)
	      {
	      eauFroide = statusRam.eauFroide;
	      if(statusRam.eauFroide)
		  {
		  screen->dispStr(14,14,"(on ) ");
		  }
	      else
		  {
		  screen->dispStr(14,14,"(off)");
		  }
	      }

	  if(eauChaude != statusRam.eauChaude)
	      {
	      eauChaude = statusRam.eauChaude;
	      if(statusRam.eauChaude)
		  {
		  screen->dispStr(14,15,"(on ) ");
		  }
	      else
		  {
		  screen->dispStr(14,15,"(off)");
		  }
	      }

	  if(valveEauFroide != (int)statusRam.valveEauFroide)
	      {
	      valveEauFroide = (int)statusRam.valveEauFroide;
	      sprintf(bufStr,"(%03d %%)",(int)statusRam.valveEauFroide);
	      screen->dispStr(22,16,bufStr);
	      }

	  if(valveEauChaude != (int)statusRam.valveEauChaude)
	      {
	      valveEauChaude = (int)statusRam.valveEauChaude;
	      sprintf(bufStr,"(%03d %%)",(int)statusRam.valveEauChaude);
	      screen->dispStr(22,17,bufStr);
	      }

	  if(valveGrosBassin != (int)statusRam.valveGrosBassin)
	      {
	      valveGrosBassin = (int)statusRam.valveGrosBassin;
	      sprintf(bufStr,"(%03d %%)",(int)statusRam.valveGrosBassin);
	      screen->dispStr(22,18,bufStr);
	      }

	  if(valvePetitBassin != (int)statusRam.valvePetitBassin)
	      {
	      valvePetitBassin = (int)statusRam.valvePetitBassin;
	      sprintf(bufStr,"(%03d %%)",(int)statusRam.valvePetitBassin);
	      screen->dispStr(22,19,bufStr);
	      }
	  }
      else  // auto
	  {
	  if(consigneNiveauGrosBassin != (int)statusRam.consigneNiveauGrosBassin)
	      {
	      consigneNiveauGrosBassin = (int)statusRam.consigneNiveauGrosBassin;
	      sprintf(bufStr,"(%03d %%)",(int)statusRam.consigneNiveauGrosBassin);
	      screen->dispStr(32,14,bufStr);
	      }

	  if(consigneNiveauPetitBassin != (int)statusRam.consigneNiveauPetitBassin)
	      {
	      consigneNiveauPetitBassin = (int)statusRam.consigneNiveauPetitBassin;
	      sprintf(bufStr,"(%03d %%)",(int)statusRam.consigneNiveauPetitBassin);
	      screen->dispStr(32,15,bufStr);
	      }

	  if(consigneTemperaturePetitBassin != (int)statusRam.consigneTemperaturePetitBassin)
	      {
	      consigneTemperaturePetitBassin = (int)statusRam.consigneTemperaturePetitBassin;
	      sprintf(bufStr,"(%02d Cel)",(int)statusRam.consigneTemperaturePetitBassin);
	      screen->dispStr(37,16,bufStr);
	      }
	  }

      // clavier
      if(clavier->kbhit())
	  {
	  car = clavier->getch();

	  switch(car)
	      {
	      case '1': // mode
		  statusRam.mode = (statusRam.mode)? 0 : 1;
		  ram->setMode(statusRam.mode);
		  break;
	      case '2': // pompe
		  statusRam.pompe = (statusRam.pompe)? 0 : 1;
		  ram->setPompe(statusRam.pompe);
		  break;
	      case '3': // arret
		  ram->init();
		  break;
	      }

	  if(mode == 1) // manuel
	      {
	      switch(car)
		  {
		  case '4': // eau froide
		      statusRam.eauFroide = (statusRam.eauFroide)? 0 : 1;
		      ram->setEauFroide(statusRam.eauFroide);
		      break;
		  case '5': // eau chaude
		      statusRam.eauChaude = (statusRam.eauChaude)? 0 : 1;
		      ram->setEauChaude(statusRam.eauChaude);
		      break;
		  case '6': // valve eau froide
		      screen->dispStr(40,16,"(0 @ 100%) : ");
		      scanf("%lf",&statusRam.valveEauFroide);
		      if( (statusRam.valveEauFroide < 0) || (statusRam.valveEauFroide > 100) )
			  statusRam.valveEauFroide = 0;
		      screen->dispStr(40,16,"             ");
		      ram->setValveEauFroide(statusRam.valveEauFroide);
		      break;
		  case '7': // valve eau chaude
		      screen->dispStr(40,17,"(0 @ 100%) : ");
		      scanf("%lf",&statusRam.valveEauChaude);
		      if( (statusRam.valveEauChaude < 0) || (statusRam.valveEauChaude > 100) )
			  statusRam.valveEauChaude = 0;
		      screen->dispStr(40,17,"             ");
		      ram->setValveEauChaude(statusRam.valveEauChaude);
		      break;
		  case '8': // valve gros bassin
		      screen->dispStr(40,18,"(0 @ 100%) : ");
		      scanf("%lf",&statusRam.valveGrosBassin);
		      if( (statusRam.valveGrosBassin < 0) || (statusRam.valveGrosBassin > 100) )
			  statusRam.valveGrosBassin = 0;
		      screen->dispStr(40,18,"             ");
		      ram->setValveGrosBassin(statusRam.valveGrosBassin);
		      break;
		  case '9': // valve petit bassin
		      screen->dispStr(40,19,"(0 @ 100%) : ");
		      scanf("%lf",&statusRam.valvePetitBassin);
		      if( (statusRam.valvePetitBassin < 0) || (statusRam.valvePetitBassin > 100) )
			  statusRam.valvePetitBassin = 0;
		      screen->dispStr(40,19,"             ");
		      ram->setValvePetitBassin(statusRam.valvePetitBassin);
		      break;
		  }
	      }
	  else   // auto
	      {
	      switch(car)
		  {
		  case '4': // consigne niveau gros bassin
		      screen->dispStr(60,14,"(0 @ 100%) : ");
		      scanf("%lf",&statusRam.consigneNiveauGrosBassin);
		      if( (statusRam.consigneNiveauGrosBassin < 0) || (statusRam.consigneNiveauGrosBassin > 100) )
			  statusRam.consigneNiveauGrosBassin = 0;
		      screen->dispStr(60,14,"             ");
		      ram->setConsigneNiveauGrosBassin(statusRam.consigneNiveauGrosBassin);
		      break;
		  case '5': // consigne niveau petit bassin
		      screen->dispStr(60,15,"(0 @ 100%) : ");
		      scanf("%lf",&statusRam.consigneNiveauPetitBassin);
		      if( (statusRam.consigneNiveauPetitBassin < 0) || (statusRam.consigneNiveauPetitBassin > 100) )
			  statusRam.consigneNiveauPetitBassin = 0;
		      screen->dispStr(60,15,"             ");
		      ram->setConsigneNiveauPetitBassin(statusRam.consigneNiveauPetitBassin);
		      break;
		  case '6': // consigne temperature petit bassin
		      screen->dispStr(60,16,"(0 @ 100 Cel) : ");
		      scanf("%lf",&statusRam.consigneTemperaturePetitBassin);
		      if( (statusRam.consigneTemperaturePetitBassin < 0) || (statusRam.consigneTemperaturePetitBassin > 100) )
			  statusRam.consigneTemperaturePetitBassin = 0;
		      screen->dispStr(60,16,"                ");
		      ram->setConsigneTemperaturePetitBassin(statusRam.consigneTemperaturePetitBassin);
		      break;
		  }
	      }
	  }
       }
    while( (car != 'q') && (car != 'Q') );

    // Destruction tâches
    ram->setMode(0);      // manuel
    sleep(1);

    if(task4)
	delete task4;
    if(control)
	delete control;
    if(alarme)
	delete alarme;
    if(ram)
	delete ram;

    // Destruction Clavier,console et horloge
    if(clavier)
	delete clavier;
    if(temps)
	delete temps;
    if(screen)
	delete screen;

    return 0;
    }

void effaceMenu(void)
    {
    screen->dispStr(1,14,"                                      ");
    screen->dispStr(1,15,"                                      ");
    screen->dispStr(1,16,"                                            ");
    screen->dispStr(1,17,"                            ");
    screen->dispStr(1,18,"                            ");
    screen->dispStr(1,19,"                            ");
    }

void menuAuto(TRam::ram_t *status)
    {
    char bufStr[50];

    screen->dispStr(1,14,"4 Consigne Niveau Gros Bassin ");
    sprintf(bufStr,"(%03d %%)",(int)status->consigneNiveauGrosBassin);
    screen->dispStr(32,14,bufStr);

    screen->dispStr(1,15,"5 Consigne Niveau Petit Bassin ");
    sprintf(bufStr,"(%03d %%)",(int)status->consigneNiveauPetitBassin);
    screen->dispStr(32,15,bufStr);

    screen->dispStr(1,16,"6 Consigne Température Petit Bassin ");
    sprintf(bufStr,"(%02d Cel)",(int)status->consigneTemperaturePetitBassin);
    screen->dispStr(37,16,bufStr);
    }

void menuManuel(TRam::ram_t *status)
    {
    char bufStr[50];

    screen->dispStr(1,14,"4 Eau Froide ");
    sprintf(bufStr,"(%s)",(status->eauFroide)? "on " : "off");
    screen->dispStr(14,14,bufStr);

    screen->dispStr(1,15,"5 Eau Chaude ");
    sprintf(bufStr,"(%s)",(status->eauChaude)? "on " : "off");
    screen->dispStr(14,15,bufStr);

    screen->dispStr(1,16,"6 Valve Eau Froide   ");
    sprintf(bufStr,"(%03d %%)",(int)status->valveEauFroide);
    screen->dispStr(22,16,bufStr);

    screen->dispStr(1,17,"7 Valve Eau Chaude   ");
    sprintf(bufStr,"(%03d %%)",(int)status->valveEauChaude);
    screen->dispStr(22,17,bufStr);

    screen->dispStr(1,18,"8 Valve Gros Bassin  ");
    sprintf(bufStr,"(%03d %%)",(int)status->valveGrosBassin);
    screen->dispStr(22,18,bufStr);

    screen->dispStr(1,19,"9 Valve Petit Bassin ");
    sprintf(bufStr,"(%03d %%)",(int)status->valvePetitBassin);
    screen->dispStr(22,19,bufStr);
    }
