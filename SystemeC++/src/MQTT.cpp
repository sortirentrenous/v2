#include "MQTT.hpp"
#include "screen.hpp"

#define MQTTCLIENT_QOS2 1

#include "MQTTClient/src/MQTTClient.h"

#define DEFAULT_STACK_SIZE -1

#include "MQTTClient/src/linux/linux.cpp"

//#include "MQTTPacket/src/MQTTConnect.h"

int arrivedcount = 0;

void messageArrived(MQTT::MessageData& md)
{
    MQTT::Message &message = md.message;

	printf("Message %d arrived: qos %d, retained %d, dup %d, packetid %d\n",
		++arrivedcount, message.qos, message.retained, message.dup, message.id);
    printf("Payload %.*s\n", message.payloadlen, (char*)message.payload);
}

TTask4::TTask4(const char *name,sharedData_t *shared,int policy,int priority,destruction_t destruction,int cpu) :
                                                        TThread(name,shared,policy,priority,destruction,cpu)
    {
    screen->dispStr(1,7,"Task 2 (CPU :  ) :");
    }

TTask4::~TTask4()
    {
    }

void TTask4::task(void)
    {
    // variable locale

    // synchronisation démarrage tâche
    signalStart();

    IPStack ipstack = IPStack();


        	    MQTT::Client<IPStack, Countdown> client = MQTT::Client<IPStack, Countdown>(ipstack);

        	    const char* hostname = "10.0.0.59";
        	    int port = 1883;
        	    int rc = ipstack.connect(hostname, port);
        		if (rc != 0)
        		    screen->dispStr(8,8, "rc from TCP connect is ..\n");

        		printf("MQTT connecting\n");
        	    MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
        	    data.MQTTVersion = 3;
        	    data.clientID.cstring = (char*)"mbed-icraggs";
        	    rc = client.connect(data);
        		if (rc != 0)
        		    printf("rc from MQTT connect is %d\n", rc);
        		screen->dispStr(12,12, "MQTT connected\n");

        		rc = client.subscribe("tables/#", MQTT::QOS2, messageArrived);
        		    if (rc != 0)
        		    	printf("rc from MQTT subscribe is %d\n", rc);
        		    MQTT::Message message;
        		        	    char buf[100];
    while(1)
	{

/*
	(int)statusRam.consigneNiveauGrosBassin
			     << ",ConsNivPB=" << (int)statusRam.consigneNiveauPetitBassin
			     << ",ConsTmpPB=" << (int)statusRam.consigneTemperaturePetitBassin
			     << ",NivGB=" << statusRam.niveauGrosBassin
			     << ",NivPB=" << statusRam.niveauPetitBassin
			     << ",TmpGB=" << statusRam.temperatureGrosBassin
			     << ",TmpPB=" << statusRam.temperaturePetitBassin
			     << ",ValveGB=" << statusRam.valveGrosBassin
			     << ",ValvePB=" << statusRam.valvePetitBassin
			     << ",ValveEC=" << statusRam.valveEauChaude
			     << ",ValveEF=" << statusRam.valveEauFroide
			     << ",EntreeEC=" << statusRam.eauChaude
			     << ",EntreeEF=" << statusRam.eauFroide
			     << ",Pompe=" << statusRam.pompe;
*/

    	    sprintf(buf, "Lol");
    	    // QoS 0


    	    sprintf(buf, "13");
    	    message.qos = MQTT::QOS0;
    	    message.retained = false;
    	    message.dup = false;
    	    message.payload = (void*)buf;
    	    message.payloadlen = strlen(buf)+1;
    	    rc = client.publish("tables/etat_ram/NivGB", message);
    		if (rc != 0)
    			printf("Error %d from sending QoS 0 message\n", rc);
    	    else while (arrivedcount == 0)
    	        client.yield(100);


    	    // QoS 1
    	//ram->getStatusRam(&statusRam);

    	    message.qos = MQTT::QOS1;
    	    message.retained = false;
    	    message.dup = false;
    	    message.payload = (void*)buf;
    	    message.payloadlen = strlen(buf)+1;
    	    rc = client.publish("tables/etat_ram/NivGB", message);
    		if (rc != 0)
    			printf("Error %d from sending QoS 1 message\n", rc);
    	    else while (arrivedcount == 1)
    	        client.yield(100);



    	    // QoS 2

    	    sprintf(buf, "13");
    	    message.qos = MQTT::QOS2;
    	    message.payloadlen = strlen(buf)+1;
    	    rc = client.publish("tables/etat_ram/NivGB", message);
    		if (rc != 0)
    			printf("Error %d from sending QoS 2 message\n", rc);
    	    while (arrivedcount == 2)
    	        client.yield(100);


/*

   	    rc = client.unsubscribe(topic);
    	    if (rc != 0)
    	        printf("rc from unsubscribe was %d\n", rc);

    	    rc = client.disconnect();
    	    if (rc != 0)
    	        printf("rc from disconnect was %d\n", rc);
    	 */

	if(thread.destruction == DESTRUCTION_SYNCHRONE)
	    {
	    // point de destruction
		//ipstack.disconnect();
	    pthread_testcancel();
	    }

	usleep(5000000);   // 500 ms
	}
    }
