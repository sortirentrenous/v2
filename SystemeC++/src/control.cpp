#include "control.hpp"
#include "screen.hpp"

TControl *control = NULL;

TControl::TControl(const char *name,sharedData_t *shared,int policy,int priority,destruction_t destruction,int noCpu) :
                                                      TThread(name,shared,policy,priority,destruction,noCpu),
                                                      /*            Kp   Ki Kd Null Gap %I   Out                    */
                                                      pidGrosBassin(2.00,0.025,0, 50,  0, 30,TPid::ZERO_TO_CENT),
                                                      /*             Kp   Ki Kd Null Gap %I   Out                   */
                                                      pidPetitBassin(2.00,0.025,0, 50,  0, 30,TPid::ZERO_TO_CENT),
                                                      /*                 Kp   Ki Kd Null Gap %I   Out               */
                                                      pidTempPetitBassin(5.00,0.1,0, 00,   0, 30,TPid::CENT_TO_MOINS_CENT)
    {
    partageRam = ram->getPartageRam();

    screen->dispStr(1,5,"Control :");
    }

TControl::~TControl()
    {
    }

void TControl::task(void)
    {
    enum chaudFroid_t
	{
	NONE  = 0,
	CHAUD = 1,
	FROID = 2
	};

    // variable locale
    char strCar[2] = {'-','\0'};
    char bufStr[50];
    chaudFroid_t chaudFroid = NONE;
    TRam::ram_t statusRam;

    // synchronisation démarrage tâche
    signalStart();

    while(1)
	{
	// attendre acquisition complété
	partageRam->synControl.take();

	//traitement
	if(strCar[0] == '|')
		strCar[0] = '-';
	else
		strCar[0] = '|';
	screen->dispStr(11,5,strCar);

	ram->getStatusRam(&statusRam);

	// niveau gros bassin
	if(statusRam.consigneNiveauGrosBassin == 0)
	    {
	    // sortie inversée (normalement ouvert)
	    ram->setValveGrosBassin(100);

	    #if 0
	    // Debug
	    sprintf(bufStr,"(0000)");
	    screen->dispStr(13,23,bufStr);
	    #endif
	    }
	else
	    {
	    pidGrosBassin.update(statusRam.consigneNiveauGrosBassin,statusRam.niveauGrosBassin);
	    // sortie inversée (normalement ouvert)
	    ram->setValveGrosBassin(100 - pidGrosBassin.getOutput());

	    #if 0
	    // Debug
	    sprintf(bufStr,"(%04.0lf)",pidGrosBassin.getError());
	    screen->dispStr(13,23,bufStr);
	    #endif
	    }

	// niveau petit bassin
	if(statusRam.consigneNiveauPetitBassin == 0)
	    {
	    // sortie directe (normalement ferme)
	    ram->setValvePetitBassin(0);

	    #if 0
	    // Debug
	    sprintf(bufStr,"(0000)");
	    screen->dispStr(13,24,bufStr);
	    #endif
	    }
	else
	    {
	    pidPetitBassin.update(statusRam.consigneNiveauPetitBassin,statusRam.niveauPetitBassin);
	    // sortie directe (normalement ferme)
	    ram->setValvePetitBassin(pidPetitBassin.getOutput());

	    #if 0
	    // Debug
	    sprintf(bufStr,"(%04.0lf)",pidPetitBassin.getError());
	    screen->dispStr(13,24,bufStr);
	    sprintf(bufStr,"(%04.0lf)",pidPetitBassin.getOutput());
	    screen->dispStr(13,25,bufStr);
	    #endif
	    }

	// temperature petit bassin
	if(statusRam.consigneTemperaturePetitBassin == 0)
	    {
	    // sortie directe (normalement ferme)
	    ram->setValveEauChaude(0);
	    // sortie inversée (normalement ouverte)
	    ram->setValveEauFroide(100);

	    #if 0
	    // Debug
	    sprintf(bufStr,"(0000)");
	    screen->dispStr(41,24,bufStr);
	    #endif
	    }
	else
	    {
	    pidTempPetitBassin.update(statusRam.consigneTemperaturePetitBassin,statusRam.temperaturePetitBassin);
	    if(pidTempPetitBassin.getError() <= 0) //refroidir
		{
		if(chaudFroid != FROID)
		    {
		    pidTempPetitBassin.clearIErrCumul();
		    // sortie directe (normalement ferme)
		    ram->setValveEauChaude(0);
		    ram->setEauChaude(0);
		    ram->setEauFroide(1);
		    chaudFroid = FROID;
		    }
		// sortie inversee (normalement ouverte)
		ram->setValveEauFroide(100 - abs(pidTempPetitBassin.getOutput()));
		}
	    else // chauffage
		{
		if(chaudFroid != CHAUD)
		    {
		    pidTempPetitBassin.clearIErrCumul();
		    // sortie inversee (normalement ouverte)
		    ram->setValveEauFroide(100);
		    ram->setEauFroide(0);
		    ram->setEauChaude(1);
		    chaudFroid = CHAUD;
		    }
		// sortie directe (normalement ferme)
		ram->setValveEauChaude(pidTempPetitBassin.getOutput());
		}

	    //#if 0
	    // Debug
	    sprintf(bufStr,"%05.0lf %04.0lf",pidTempPetitBassin.getError(),pidTempPetitBassin.getOutput()/*,pidTempPetitBassin.getIErrCumul()*/);

	    //sprintf(bufStr,"(%04.0lf)",pidTempPetitBassin.getError());
	    screen->dispStr(41,34,bufStr);
	    //#endif
	    }

	if(thread.destruction == DESTRUCTION_SYNCHRONE)
	    {
	    // point de destruction
	    pthread_testcancel();
	    }
	}
    }
