
var mosca = require('mosca'); // Middleware pour la création d'un broker
var Client = require('mariasql'); //Middleware pour MariaDB


var ascoltatore = { //Parametres du serveur redis
  type: 'redis',
  redis: require('redis'), //Syssteme d'association clef/valeur noSQL
  db: 12,
  port: 6379,
  return_buffers: true, // to handle binary payloads
  host: "localhost"
};

var moscaSettings = { //Parametres de notre broker
  port: 1883,
  backend: ascoltatore,
  persistence: {
    factory: mosca.persistence.Redis
  }
};

var server = new mosca.Server(moscaSettings); //Création du broker

server.on('ready', setup); //Le broker est prêt

server.on('clientConnected', function(client) { //Un nouveau client viens de se connecter
    console.log('client connected', client.id);     
});

// fired when a message is received
server.on('published', function(packet, client) {//Un client vient de publier quelque chose
  console.log('Published', packet);
  if ((packet.topic.indexOf('tables/etat_') > -1) ||
   (packet.topic.indexOf('tables/set_alarmes/') > -1)) {
    var tabSplit = packet.topic.split('/');
    SQL('UPDATE ' + tabSplit[1] + ' SET ' + tabSplit[2] + ' = :val WHERE 1', {val: packet.payload.toString()}, function(rows) {
      
    });
  } else if (packet.topic.indexOf('tables/cmd_ram/nouvelleCmd/') > -1) {
    var tabSplit = packet.payload.toString().split("/");
    console.log(tabSplit);
    
    SQL('INSERT INTO journal (Type, UserID, ReqTime, Info) VALUES (:Type, :UserID, NOW(), :Info)',
          {Type: 0, UserID: tabSplit[3], Info: 'INFOS'}, function(rows0) {
      SQL('INSERT INTO cmd_ram (log_id, cmd, p1, p2) VALUES (:log_id, :cmd, :p1, :p2)',
            {log_id: rows0['info']['insertId'], cmd: tabSplit[0], p1: tabSplit[1], p2: tabSplit[2]}, function(rows1) {
        server.publish({topic:'tables/cmd_ram/cmdAdd',
          payload:rows1['info']['insertId'] + '/' + rows0['info']['insertId']
          + '/' + tabSplit[0] + '/' + tabSplit[1] + '/' + tabSplit[2]});
        SQL('SELECT ReqTime, DoneTime FROM journal WHERE LogID = :LogID ', {LogID:rows0['info']['insertId']}, function(rows2) {
          server.publish({topic:'tables/journal/logAdd',
                          payload: rows0['info']['insertId'] + '/0/' + tabSplit[3]
                          + '/' + rows2[0].ReqTime + '/' + rows2[0].DoneTime + '/' + 'INFOS'});
        });
      });
    });
  } else if (packet.topic.indexOf('tables/cmd_melangeur/nouvelleCmd/') > -1) {
    var tabSplit = packet.payload.toString().split("/");
    console.log(tabSplit);
    
    SQL('INSERT INTO journal (Type, UserID, ReqTime, Info) VALUES (:Type, :UserID, NOW(), :Info)',
          {Type: 0, UserID: tabSplit[4], Info: 'INFOS'}, function(rows0) {
      SQL('INSERT INTO cmd_melangeur (log_id, cmd, p1, p2, p3) VALUES (:log_id, :cmd, :p1, :p2, :p3)',
            {log_id: rows0['info']['insertId'], cmd: tabSplit[0], p1: tabSplit[1], p2: tabSplit[2], p3: tabSplit[3]}, function(rows1) {
        server.publish({topic:'tables/cmd_melangeur/cmdAdd',
          payload:rows1['info']['insertId'] + '/' + rows0['info']['insertId']
          + '/' + tabSplit[0]+ '/' + tabSplit[1] + '/' + tabSplit[2] + '/' + tabSplit[3]});
        SQL('SELECT ReqTime, DoneTime FROM journal WHERE LogID = :LogID ', {LogID:rows0['info']['insertId']}, function(rows2) {
          server.publish({topic:'tables/journal/logAdd',
                          payload: rows0['info']['insertId'] + '/0/' + tabSplit[4]
                          + '/' + rows2[0].ReqTime + '/' + rows2[0].DoneTime + '/' + 'INFOS'});
        });
      });
    });
  } else if (packet.topic.indexOf('tables/alarmes/nouvelleAlarme/') > -1) {
    console.log('Nouvelle alarmes...');
    var tabSplit = packet.payload.toString().split("/");
    console.log(tabSplit);

    SQL('INSERT INTO alarmes (Type, Val, DateTime) VALUES (:Type, :Val, NOW())',
            {Type:tabSplit[0], Val: tabSplit[1]}, function(rows0) {
      SQL('SELECT * FROM alarmes WHERE AlarmeID = :AlarmeID ',
            {AlarmeID:rows0['info']['insertId']}, function(rows1) {        
        SQL('INSERT INTO journal (Type, UserID, ReqTime, Info) VALUES (:Type, :UserID, NOW(), :Info)',
          {Type: 1, UserID: tabSplit[2], Info: 'INFOS'}, function(rows2) {
          SQL('SELECT ReqTime, DoneTime FROM journal WHERE LogID = :LogID ', {LogID:rows2['info']['insertId']}, function(rows3) {
            server.publish({topic:'tables/journal/logAdd',
                            payload: rows2['info']['insertId'] + '/1/' + tabSplit[2]
                            + '/' + rows3[0].ReqTime + '/' + rows3[0].DoneTime + '/' + 'INFOS'});
            server.publish({topic:'tables/alarmes/alarmeAdd',
                          payload:rows1[0].AlarmeID + '/' + rows1[0].Type
                          + '/' + rows1[0].Val + '/' + rows1[0].DateTime + '/' + rows3[0].ReqTime + '/' + rows2['info']['insertId'] + '/' + 'INFOS'});
          });
        });
      });
    });
  } else if (packet.topic.indexOf('tables/journal/requestAcquittement') > - 1) {
    var tabSplit = packet.payload.toString().split("/");
    console.log(tabSplit);
    SQL('UPDATE journal SET DoneTime = NOW(), Info = CONCAT(Info, :msg) WHERE LogID = :LogID',
        {LogID: tabSplit[0], msg: '-Info:' + tabSplit[1]}, function(rows0) {});
      SQL('SELECT DoneTime, Info FROM journal WHERE LogID = :LogID ', {LogID: tabSplit[0]}, function(rows1) {
        server.publish({topic:'tables/journal/updateAcquittement',
                        payload: tabSplit[0] + '/' + rows1[0].DoneTime + '/' + rows1[0].Info});
    });
  }
});


// fired when the mqtt server is ready
function setup() {
  console.log('Mosca server is up and running');
}

function SQL(requete, data, utiliser_resultats) { // Exeute une requete SQL pour MariaDB
    console.log(requete);
    var c = new Client({
        host: '127.0.0.1',
        user: 'root',
        password: '',
        db: 'RAM'
    });
    c.query(requete, data,
        function(err, rows) {
        if (err)
            throw err;
        utiliser_resultats(rows)
    });
    c.end();
}
