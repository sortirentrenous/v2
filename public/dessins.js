function dessinerBalance() {	
    		var canvas = document.getElementById('mon_canvas');
	        if(!canvas)
	        {
	            //alert("Impossible de récupérer le canvas");
	            return;
	        }

	        var context = canvas.getContext('2d');
	        if(!context)
	        {
	            //alert("Impossible de récupérer le context du canvas");
	            return;
	        }
	        var angleMot = 0;

	        setInterval(function() {
		        context.clearRect(0, 0, canvas.width, canvas.height);

		        

		        context.lineWidth = 3;
		        context.strokeStyle = "#000000";

		        //base balance
		        context.beginPath();//On démarre un nouveau tracé
		        context.moveTo(75, 400);//On se déplace au coin inférieur gauche
		        context.lineTo(125, 250);
		        context.lineTo(375, 250);
		        context.lineTo(425, 400);
		        context.lineTo(74, 400);
		        context.stroke();//On trace seulement les lignes.


		        //Ecran balance
		        context.fillStyle = "rgba(0, 0, 255, 0.25)";
		        context.beginPath();//On démarre un nouveau tracé
		        context.moveTo(150, 375);//On se déplace au coin inférieur gauche
		        context.lineTo(150, 325);
		        context.lineTo(350, 325);
		        context.lineTo(350, 375);
		        context.lineTo(150, 375);
		        context.fill();//On trace seulement les lignes.
		        context.closePath();

		        //Affichage ecran balance
		        context.fillStyle = "#000000";
		        context.font = "25px Helvetica";
		        var poid = document.getElementById('tables_etat_balance_poids').innerHTML;
		        context.fillText(poid, 225, 355);
		        var unite = document.getElementById('tables_etat_balance_type').innerHTML;
		        (unite == 1) ? unite = 'KG' : unite = 'LB';
		        context.fillText(unite, 285, 355);
		        context.font = "10px Helvetica";
		        
		        var tare = document.getElementById('tables_etat_balance_tare').innerHTML;
		        context.fillText('Tare : ' + tare + unite, 175, 370);
		        
		        var mode = document.getElementById('tables_etat_melangeur_Mode').innerHTML;
		        (mode == 0) ? mode = 'AUTO' : mode = 'MANUEL';
		        context.fillText('Mode : ' + mode, 250, 370);

		        //Plateau balance
		        context.beginPath(); //On démarre un nouveau tracé.
		        context.arc(125, 175, 50, 3.14, 1.57, true); //On trace la courbe délimitant notre forme
		        context.lineTo(375, 225);
		        context.arc(375, 175, 50, 1.57, 0, true); //On trace la courbe délimitant notre forme
		        context.stroke(); //On utilise la méthode fill(); si l'on veut une forme pleine
		        context.closePath();
/*
		        context.beginPath(); //On démarre un nouveau tracé.
		        context.rect(50, 10, 400, 150);
		        context.stroke();
		        context.closePath();
*/
		        var motA = document.getElementById('tables_etat_melangeur_MotA').innerHTML;
		        if (motA == 0) { //Moteur A OFF
		        	context.beginPath();
					context.arc(125, 75, 50, 0, Math.PI*2, true);
					context.stroke();
					context.closePath();
		        } else {
		        	context.fillStyle = "#FF00FF";
		        	context.strokeStyle = "#FF00FF";
		        	context.beginPath();
					context.arc(125, 75, 50, angleMot, angleMot + 2, true);
					context.stroke();
					context.closePath();
					angleMot += 0.1;
		        }
		        context.font = "21px Helvetica";
		        context.fillText('Mot A ', 97, 82);

		        var motB = document.getElementById('tables_etat_melangeur_MotB').innerHTML;
		        if (motB == 0) { //Moteur B OFF
		        	context.beginPath();
		        	context.strokeStyle = "#000000";
		        	context.fillStyle = "#000000";
					context.arc(250, 75, 50, 0, Math.PI*2, true);
					context.stroke();
					context.closePath();
		        } else {
		        	context.beginPath();
		        	context.fillStyle = "#FF00FF";
		        	context.strokeStyle = "#FF00FF";
					context.arc(250, 75, 50, angleMot, angleMot + 2, true);
					context.stroke();
					context.closePath();
					angleMot += 0.1;
		        }
		        context.font = "21px Helvetica";
		        context.fillText('Mot B ', 223, 82);

		        var motC = document.getElementById('tables_etat_melangeur_MotC').innerHTML;
		        if (motC == 0) { //Moteur C OFF
		        	context.beginPath();
		        	context.strokeStyle = "#000000";
		        	context.fillStyle = "#000000";
					context.arc(375, 75, 50, 0, Math.PI*2, true);
					context.stroke();
					context.closePath();
		        } else {
		        	context.beginPath();
		        	context.fillStyle = "#FF00FF";
		        	context.strokeStyle = "#FF00FF";
					context.arc(375, 75, 50, angleMot, angleMot + 2, true);
					context.stroke();
					context.closePath();
					angleMot += 0.1;
		        }
		        context.font = "21px Helvetica";
		        context.fillText('Mot C ', 350, 82);


					        
		    },1000/15);
    	}

    	function dessinerPowemeter() {
    		console.log('MDr');
    		var canvas = document.getElementById('mon_canvas_powermeter');
	        if(!canvas)
	        {
	            //alert("Impossible de récupérer le canvas");
	            return;
	        }

	        var context = canvas.getContext('2d');
	        if(!context)
	        {
	            //alert("Impossible de récupérer le context du canvas");
	            return;
	        }
	        var angleMot = 0;

	        setInterval(function() {
		        context.clearRect(0, 0, canvas.width, canvas.height);
		        context.lineWidth = 3;
		        context.font = "21px Helvetica";
		        context.fillText('Charles mtovie toi ', 223, 82);
			        
		    },1000/15);
    	}

    	function dessinerRAM() {
    		var canvas = document.getElementById('mon_canvas_RAM');
	        if(!canvas)
	        {
	            //alert("Impossible de récupérer le canvas");
	            return;
	        }

	        var context = canvas.getContext('2d');
	        if(!context)
	        {
	            //alert("Impossible de récupérer le context du canvas");
	            return;
	        }
	        var angleMot = 0;

	        setInterval(function() {
	        	var valveEauFroide = document.getElementById('tables_etat_ram_ValveEauFroide').innerHTML;
	        	var pompe = document.getElementById('tables_etat_ram_Pompe').innerHTML;
	        	var consNivGB = document.getElementById('tables_etat_ram_ConsNivGB').innerHTML;
	        	var niveauGB = document.getElementById('tables_etat_ram_NivGB').innerHTML;
	        	var consNivPB = document.getElementById('tables_etat_ram_ConsNivPB').innerHTML;
	        	var valveGB = document.getElementById('tables_etat_ram_ValveGB').innerHTML;
	        	var consTempPB = document.getElementById('tables_etat_ram_ConsTmpPB').innerHTML;
	        	var niveauPB = document.getElementById('tables_etat_ram_NivPB').innerHTML;
	        	var valvePB = document.getElementById('tables_etat_ram_ValvePB').innerHTML;
	        	var tempPB = document.getElementById('tables_etat_ram_TmpPB').innerHTML;
	        	var valveEauChaude = document.getElementById('tables_etat_ram_ValveEauChaude').innerHTML;
		        var valveEntreeEauChaude = document.getElementById('tables_etat_ram_ValveEntreeEauChaude').innerHTML;
		        var valveEntreeEauFroide = document.getElementById('tables_etat_ram_ValveEntreeEauFroide').innerHTML;

		        context.clearRect(0, 0, canvas.width, canvas.height);

		        //Circuit eau normal
		        context.lineWidth = 8;
		        context.beginPath();
		        context.strokeStyle = "#00a8ff";
		        context.moveTo(75, 260);
				context.lineTo(75, 75);
				context.lineTo(420, 75);
				context.lineTo(420, 135);
				context.moveTo(420, 165);
				context.lineTo(420, 250);
				context.moveTo(240, 75);
				context.lineTo(240, 135);
				context.moveTo(240, 165);
				context.lineTo(240, 200);		
				context.moveTo(460, 450);
				context.lineTo(460, 485);
				context.lineTo(75, 485);
				context.lineTo(75, 340);
				context.moveTo(240, 450);
				context.lineTo(240, 485);
				context.stroke();
				context.closePath();

				
				//Circuit eau froide
				context.beginPath();
		        context.strokeStyle = "blue";
		        context.moveTo(125, 20);
		        if (valveEntreeEauFroide == 0) {
		        	context.lineTo(195, 20);
					context.lineTo(220, 8);
					context.moveTo(220, 20);
		        }
				context.lineTo(500, 20);
				context.lineTo(500, 135);
				context.moveTo(500, 165);
				context.lineTo(500, 250);
				context.stroke();
				context.closePath();

				//Circuit eau chaude
				context.beginPath();
		        context.strokeStyle = "red";
		        context.moveTo(125, 45);//On se déplace au coin inférieur gauche
		        if (valveEntreeEauChaude == 0) {
		        	context.lineTo(195, 45);
					context.lineTo(220, 33);
					context.moveTo(220, 45);
		        }
				context.lineTo(460, 45);
				context.lineTo(460, 135);
				context.moveTo(460, 165);//On se déplace au coin inférieur gauche
				context.lineTo(460, 250);
				context.stroke();
				context.closePath();

		        context.lineWidth = 3;
		        
		        if (pompe == 0) { //Moteur C OFF
		        	context.beginPath();
		        	context.strokeStyle = "#000000";
		        	context.fillStyle = "#000000";
					context.arc(75, 300, 40, 0, Math.PI*2, true);
					context.stroke();
					context.closePath();
		        } else {
		        	context.beginPath();
		        	context.fillStyle = "#00a8ff";
		        	context.strokeStyle = "#00a8ff";
					context.arc(75, 300, 40, angleMot, angleMot + 2, true);
					context.stroke();
					context.closePath();
					angleMot += 0.1;
		        }
		        context.font = "18px Helvetica";
		        context.fillText('Pompe', 48, 308);

		        //Grand Bassin
		        context.beginPath(); //On démarre un nouveau tracé.
		        context.strokeStyle = "#000000";
		        context.rect(150, 200, 175, 250);
		        context.stroke();
		        context.closePath();

				
				//Grand Bassin eau
		        context.beginPath(); //On démarre un nouveau tracé.
		        context.fillStyle = "#00a8ff";
		        context.rect(151, 201 + (100 - niveauGB) * 2.48, 173, niveauGB * 2.48);
		        context.fill();
		        context.closePath();

		        //Gand Bassin text
		        context.fillStyle = "black";
		        context.font = "18px Helvetica";
		        context.fillText('Niveau', 210, 385);
		        context.fillText(niveauGB + '%', 225, 415);


		        context.fillStyle = "blue";
		        context.font = "18px Helvetica";
		        context.fillText('Eau froide', 25, 25);

		        context.fillStyle = "red";
		        context.fillText('Eau chaude', 25, 50);

		    	context.font = "14px Helvetica";
		        context.fillStyle = "black";
		        
		        
		        context.fillText(consNivGB + '%', 334, 469);

		        
		        context.fillText(consNivPB + '%', 532, 469);

		        
		        context.fillText(consTempPB + '°C', 569, 469);

		        //Petit Bassin
		        context.beginPath(); //On démarre un nouveau tracé.
		        context.strokeStyle = "#000000";
		        context.rect(400, 250, 125, 200);
		        context.stroke();
		        context.closePath();

				
		        //Petit Bassin eau
		        context.beginPath(); //On démarre un nouveau tracé.
		        context.fillStyle = "#00a8ff";
		        context.rect(401, 251 + (100 - niveauPB) * 1.98, 123, niveauPB * 1.98);
		        context.fill();
		        context.closePath();

		        //Petit Bassin text
		        context.fillStyle = "black";
		        context.font = "18px Helvetica";
		        context.fillText('Température', 412, 325);
		        
		        context.fillText(tempPB + '°C', 445, 355);
		        context.fillText('Niveau', 440, 385);
		        context.fillText(niveauPB + '%', 450, 415);


		        context.strokeStyle = "#00a8ff";
		        context.fillStyle = "#00a8ff";
		        if (valveGB == 0) {
		        	context.beginPath();
					context.arc(240, 150, 15, 0, Math.PI*2, true);
					context.stroke();
					context.closePath();
		        } else {
		        	context.beginPath();
					context.arc(240, 150, 15, angleMot, angleMot + 2, true);
					context.stroke();
					context.closePath();
					angleMot += 0.1;
		        }
		        context.font = "10px Helvetica";
		        context.fillText(valveGB + '%', 230, 153);
		        
		        context.strokeStyle = "#00a8ff";
		        context.fillStyle = "#00a8ff";
		        if (valvePB == 0) {
		        	context.beginPath();
					context.arc(420, 150, 15, 0, Math.PI*2, true);
					context.stroke();
					context.closePath();
		        } else {
		        	context.beginPath();
					context.arc(420, 150, 15, angleMot, angleMot + 2, true);
					context.stroke();
					context.closePath();
					angleMot += 0.1;
		        }
		        context.font = "10px Helvetica";
		        context.fillText(valvePB + '%', 410, 153);

		        
		        if (valveEauChaude == 0 || valveEntreeEauChaude == 0) {
		        	context.beginPath();
		        	context.fillStyle = "red";
		        	context.strokeStyle = "red";
					context.arc(460, 150, 15, 0, Math.PI*2, true);
					context.stroke();
					context.closePath();
					angleMot += 0.1;
					context.font = "10px Helvetica";
		        	context.fillText(valveEauChaude + '%', 450, 153);
		        } else {
		        	context.beginPath();
		        	context.fillStyle = "red";
		        	context.strokeStyle = "red";
					context.arc(460, 150, 15, angleMot, angleMot + 2, true);
					context.stroke();
					context.closePath();
					angleMot += 0.1;
					context.font = "10px Helvetica";
		        	context.fillText(valveEauChaude + '%', 450, 153);
		        }

		        
		        if (valveEntreeEauFroide == 0 || valveEauFroide == 0) {
		        	context.beginPath();
		        	context.fillStyle = "blue";
		        	context.strokeStyle = "blue";
					context.arc(500, 150, 15, 0, Math.PI*2, true);
					context.stroke();
					context.closePath();
					angleMot += 0.1;
					context.font = "10px Helvetica";
		        	context.fillText(valveEauFroide + '%', 490, 153);
		        } else {
		        	context.beginPath();
		        	context.fillStyle = "blue";
		        	context.strokeStyle = "blue";
					context.arc(500, 150, 15, angleMot, angleMot + 2, true);
					context.stroke();
					context.closePath();
					angleMot += 0.1;
					context.font = "10px Helvetica";
		        	context.fillText(valveEauFroide + '%', 490, 153);
		        }







			        
		    },1000/15);
    	}
