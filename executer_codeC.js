const mqtt = require('mqtt') // Charle le middlware pour MQTT
var cp = require('child_process'); // Charge le middleware execute une commande linux (Pas besoin de le NPM)

const client = mqtt.connect('mqtt://localhost/');

console.log('Execution du script de la RPI...');

client.publish('rpy/connected', 'true');
/*
client.on('connect', () => {  
	client.subscribe('rpy/connected');
})*/

executeCmd('./a.out PremierArg DeuxiemeArg', function (stdout) {
	//Traitement du resultat
});


function executeCmd(cmd, next) {
	cp.exec(cmd, function(err, stdout, stderr) {  // par exemple un ls, mais tu y met l’exécutable que tu veux
	    if (err) throw err;
	    if (stderr) console.warn(stderr);
	    console.log(stdout);
	    next(stdout);
	});
}